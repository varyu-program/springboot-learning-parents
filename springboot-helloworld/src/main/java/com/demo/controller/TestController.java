package com.demo.controller;

import com.demo.config.MyConfig1;
import com.demo.config.MyConfig2;
import com.demo.config.MyConfig3;
import com.demo.model.UserInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/app")
@Slf4j
public class TestController {

    @RequestMapping("/helloworld")
    public String testDemo() {
        return "Hello World!";
    }

    @RequestMapping("/helloworld2")
    public String testDemo2() {
        return "Hello World2!";
    }

    @RequestMapping("/helloworld3")
    public String testDemo3() {
        return "Hello World4!";
    }

    @Autowired
    ObjectMapper objectMapper;

    @RequestMapping("/getUser")
    public UserInfo gerUser() throws JsonProcessingException {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName("zhangsan");
        userInfo.setPassword("zhangsan123");
        userInfo.setAge(18);
        return userInfo;
    }

    @Autowired
    MyConfig1 myConfig1;
    @Autowired
    MyConfig2 myConfig2;
    @Autowired
    MyConfig3 myConfig3;


    @RequestMapping("/getMyconfig")
    public void getMyconfig() throws IOException {
        log.info("Myconfig1 姓名：{}，年龄：{}，住址：{}", myConfig1.getName(), myConfig1.getAge(), myConfig1.getAddress());
        log.info("Myconfig2 姓名：{}，年龄：{}，住址：{}", myConfig2.getName(), myConfig2.getAge(), myConfig2.getAddress());
        log.info("Myconfig3 姓名：{}，年龄：{}，住址：{}", myConfig3.getName(), myConfig3.getAge(), myConfig3.getAddress());
    }

    @RequestMapping("/getMyconfig4")
    public void getMyconfig4() {
        //通过这种方式获取文件流
        try (InputStream inputStream = ClassLoader.getSystemResourceAsStream("test.properties")) {
            Properties prop = new Properties();
            //指定编码为UTF-8以防中文乱码
            prop.load(new InputStreamReader(inputStream, "UTF-8"));
            String name = prop.getProperty("test.name");
            String age = prop.getProperty("test.age");
            String address = prop.getProperty("test.address");
            log.info("Myconfig4 姓名：{}，年龄：{}，住址：{}", name, age, address);
        } catch (Exception e) {
            log.error("获取配置文件失败", e);
        }
    }

    @RequestMapping("/getMyconfig5")
    public void getMyconfig5() {
        try {
            //注意这里的test只有文件名不带类型
            ResourceBundle resource = ResourceBundle.getBundle("test");
            String name = changeToUTF8(resource.getString("test.name"));
            String age = changeToUTF8(resource.getString("test.age"));
            String address = changeToUTF8(resource.getString("test.address"));
            log.info("Myconfig5 姓名：{}，年龄：{}，住址：{}", name, age, address);
        } catch (Exception e) {
            log.error("获取配置文件失败", e);
        }
    }

    /**
     * 将原来编码为8859-1字符串转为UTF-8编码
     *
     * @param str
     * @return
     */
    private String changeToUTF8(String str) throws UnsupportedEncodingException {
        if (str == null) {
            return null;
        }
        return new String(str.getBytes("ISO8859-1"));
    }
}