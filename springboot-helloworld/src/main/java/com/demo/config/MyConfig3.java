package com.demo.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * 获取自定义配置方法二
 */
@Component
@Configuration
public class MyConfig3 {
    @Autowired
    Environment env;

    public String getName() {
        return env.getProperty("test.name");
    }

    public Integer getAge() {
        return env.getProperty("test.age", Integer.class);
    }

    public String getAddress() {
        return env.getProperty("test.address");
    }
}
