package com.demo.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取自定义配置方法二
 */
@Data
@Component
public class MyConfig2 {
    @Value("${test.name}")
    private String name;
    @Value("${test.age}")
    private Integer age;
    @Value("${test.address}")
    private String address;
}
