package com.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * 获取properties文件中的配置信息方式
 */
@Component
@Configuration
public class MyConfig4 {
    @Autowired
    Environment env;

    public String getName() {
        return env.getProperty("test.name");
    }

    public Integer getAge() {
        return env.getProperty("test.age", Integer.class);
    }

    public String getAddress() {
        return env.getProperty("test.address");
    }
}
