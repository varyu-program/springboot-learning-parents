package com.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 获取自定义配置方法一
 */
@Data
@Component
@ConfigurationProperties(prefix = "test")
public class MyConfig1 {
    private String name;
    private Integer age;
    private String address;
}
