package com.demo.model;

import lombok.Data;

@Data
public class UserInfo {
    private String userName;
    private String password;
    private Integer age;
}
