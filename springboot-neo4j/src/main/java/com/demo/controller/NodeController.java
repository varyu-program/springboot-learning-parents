package com.demo.controller;


import com.demo.base.Result;
import com.demo.entity.Node;
import com.demo.service.INodeService;
import com.demo.util.nlp.MainPart;
import com.demo.util.nlp.MainPartExtractor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * com.demo.controller 测试
 */
@RestController
@RequestMapping("/node")
@Slf4j
public class NodeController {

    /**
     * 构造方法注入
     */
    @Autowired
    INodeService nodeService;


    /**
     * 保存数据
     *
     * @return
     */
    @GetMapping("/save")
    public Result save(@RequestParam("name") String name,
                       @RequestParam("title") String title) {
        Node node = new Node();
        node.setName(name);
        nodeService.save(node);
        return new Result().ok(node);
    }

    /**
     * 保存数据
     *
     * @return
     */
    @GetMapping("/executeCypherQuery")
    public Result executeCypherQuery(@RequestParam("name") String username) {
        String cypherQuery = "MATCH (s:Student {name: $username}) RETURN s";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("username", username);
        org.neo4j.ogm.model.Result result = nodeService.executeCypherQuery(cypherQuery, parameters);
        return new Result().ok(result);
    }


    /**
     * 保存数据
     *
     * @return
     */
    @GetMapping("/saveProperties")
    public Result save(@RequestParam("name") String name,
                       @RequestParam("title") String title,
                       @RequestParam("address") String address) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("address", address);
        properties.put("title", title);
        Node node = new Node();
        node.setName(name);
        node.setProperties(properties);
        nodeService.save(node);
        return new Result().ok(node);
    }

    /**
     * 查看所有数据
     *
     * @return
     */
    @GetMapping("/list")
    public Result list() {
        // 返回所有
        List<Node> list = nodeService.getAll();
        return new Result().ok(list);
    }


    /**
     * 查看所有数据
     *
     * @return
     */
    @GetMapping("/bind")
    public Result bind(@RequestParam("startNode") long startNode,
                       @RequestParam("endNode") long endNode,
                       @RequestParam("relationName") String relationName) {
        // 返回所有
        nodeService.bind(startNode, endNode, relationName);
        return new Result().ok("{}");
    }

    @GetMapping("/testCaseArray")
    public Result testCaseArray() {
        String[] testCaseArray = {
                "我一直很喜欢你",
                "你被我喜欢",
                "美丽又善良的你被卑微的我深深的喜欢着……",
                "小米公司主要生产智能手机",
                "他送给了我一份礼物",
                "这类算法在有限的一段时间内终止",
                "如果大海能够带走我的哀愁",
                "天青色等烟雨，而我在等你",
                "我昨天看见了一个非常可爱的小孩"
        };
        StringBuilder sBuilder = new StringBuilder();
        for (String testCase : testCaseArray) {
            MainPart mp = MainPartExtractor.getMainPart(testCase);
            sBuilder.append(mp.toString()).append("</p>");
            log.info(mp.toString());
        }
        return new Result().ok(sBuilder.toString());
    }

}
