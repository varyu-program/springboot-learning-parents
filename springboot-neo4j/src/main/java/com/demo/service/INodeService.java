package com.demo.service;

import com.demo.entity.Node;
import org.neo4j.ogm.model.Result;

import java.util.List;
import java.util.Map;

/**
 * 节点处理服务层
 */
public interface INodeService {

    /**
     * 保存节点
     *
     * @param node
     * @return
     */
    Node save(Node node);

    /**
     * 获取节点
     *
     * @return
     */
    List<Node> getAll();

    /**
     * 创建关联
     *
     * @param startNodeId
     * @param endNodeId
     * @param relationName
     */
    void bind(Long startNodeId, Long endNodeId, String relationName);

    /**
     * 直接执行 cypherQuery 查询语句，使用示例如下：
     *
     * String cypherQuery = "MATCH (s:Student {name: $name}) RETURN s";
     * Map<String, Object> parameters = new HashMap<>();
     * parameters.put("name", "Alice");
     *
     * Result result = cypherQueryRepository.executeCypherQuery(cypherQuery, parameters);
     *
     * @param cypherQuery
     * @param parameters
     * @return
     */
    Result executeCypherQuery(String cypherQuery, Map<String, Object> parameters);
}
