package com.demo.entity;

import lombok.Data;
import org.neo4j.ogm.annotation.*;

import java.util.Map;


/**
 * 人员实体，通过在实体类上添加@NodeEntity表明它是图中的一个节点实体，在属性上添加@Property代表它是节点中的具体属性
 */
@Data
@NodeEntity(label = "Person")
public class Node {

    /**
     * 节点唯一ID
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * 节点名称
     */
    @Property(name = "name")
    private String name;

    /**
     * 节点属性
     */
    @Properties
    private Map<String, Object> properties;

    public void addProperty(String key, Object value) {
        properties.put(key, value);
    }

    public void removeProperty(String key) {
        properties.remove(key);
    }

    public Object getProperty(String key) {
        return properties.get(key);
    }
}