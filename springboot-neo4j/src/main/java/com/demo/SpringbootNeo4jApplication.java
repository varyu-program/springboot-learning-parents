package com.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "com.demo.com.demo.mapper")
public class SpringbootNeo4jApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootNeo4jApplication.class, args);
	}

}
