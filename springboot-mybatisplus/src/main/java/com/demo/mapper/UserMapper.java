package com.demo.mapper;

import com.demo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Leo825
 * @since 2022-07-05
 */
public interface UserMapper extends BaseMapper<User> {

}
