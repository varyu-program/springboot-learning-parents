package com.demo.controller;

import com.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/session")
@Slf4j
public class SpringSessionController {

    @RequestMapping("/putsession")
    public String putSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        log.info("sessionClass为：{}", session.getClass());
        log.info("sessionId为：{}", session.getId());
        session.setAttribute("user", "i am leo825");
        return "putsession success";
    }
}
