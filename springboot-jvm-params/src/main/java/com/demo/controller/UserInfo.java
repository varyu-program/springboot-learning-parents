package com.demo.controller;

import lombok.Data;

@Data
public class UserInfo {
    private String name;
    private Integer age;
    private String address;
}
