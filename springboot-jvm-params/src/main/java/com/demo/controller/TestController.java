package com.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/jvm")
@Slf4j
public class TestController {

    @GetMapping("/counts")
    public void jvmTest(@RequestParam(value = "counts") Integer counts) throws InterruptedException {
        long begin = System.currentTimeMillis();
        log.info("开始执行jvm测试。。。");
        log.info("循环的次数为：{}", counts);
        ArrayList<UserInfo> userInfos = new ArrayList<>();
        for (int i = 0; i < counts; i++) {
            UserInfo userInfo = new UserInfo();
            userInfo.setAge(i);
            userInfos.add(userInfo);
            TimeUnit.MILLISECONDS.sleep(2);
        }
        long end = System.currentTimeMillis();
        log.info("jvm测试程序结束，耗时：{} 毫秒，数组长度：{}", end - begin, userInfos.size());

    }
}
