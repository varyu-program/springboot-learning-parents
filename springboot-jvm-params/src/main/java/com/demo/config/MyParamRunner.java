package com.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@Slf4j
public class MyParamRunner implements CommandLineRunner {

    /**
     * 自定义测试参数
     */
    @Value("${myParam}")
    String myParam;

    /**
     * 获取环境变量参数
     */
    @Autowired
    Environment environment;

    @Override
    public void run(String... args) throws Exception {
        log.info("本地设置的参数myParam为：{}", myParam);
        log.info(environment.toString());
        log.info(String.valueOf(Arrays.asList(environment.getActiveProfiles())));
    }
}
