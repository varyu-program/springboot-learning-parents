package main;

import domain.Request;
import service.impl.HandlerA;
import service.impl.HandlerB;
import service.impl.HandlerC;

public class Client {
    public static void main(String[] args) {
        HandlerA handlerA = new HandlerA();
        HandlerB handlerB = new HandlerB();
        HandlerC handlerC = new HandlerC();
        handlerA.setNextHandler(handlerB);
        handlerB.setNextHandler(handlerC);
        //这个请求A可以处理
        Request request = new Request("Type1", "请求1");
        //这个请求A处理发现处理不了,则会交给B处理
        Request request2 = new Request("Type2", "请求2");
        //这个请求A处理发现处理不了,则会交给B处理.B处理不了则交给C处理 ~一次类推
        Request request3 = new Request("Type3", "请求3");
        Request request4 = new Request("Type4", "请求4");
        handlerA.handleRequest(request);
        handlerA.handleRequest(request2);
        handlerA.handleRequest(request3);
        handlerA.handleRequest(request4);
    }
}
