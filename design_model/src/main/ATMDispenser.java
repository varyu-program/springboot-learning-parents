package main;

import domain.Currency;
import service.DispenseChain;
import service.impl.Dollar10Dispenser;
import service.impl.Dollar20Dispenser;
import service.impl.Dollar50Dispenser;

public class ATMDispenser {
    private DispenseChain c1;

    public ATMDispenser() {
        // initialize the chain
        this.c1 = new Dollar50Dispenser();
        DispenseChain c2 = new Dollar20Dispenser();
        DispenseChain c3 = new Dollar10Dispenser();
        // set the chain of responsibility
        c1.setNextChain(c2);
        c2.setNextChain(c3);
    }

    public void dispenseMoney(Currency currency) {
        if (currency.getAmount() % 10 != 0) {
            System.out.println("Amount should be in multiple of 10.");
            return;
        }

        // process the request
        c1.dispense(currency);
    }

    public static void main(String[] args) {
        ATMDispenser atmDispenser = new ATMDispenser();
        Currency currency = new Currency(40);
        atmDispenser.dispenseMoney(currency);
    }
}
