package service.impl;

import domain.Request;
import service.Handler;

public class HandlerC implements Handler{
    private Handler nextHandler;

    @Override
    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    @Override
    public void handleRequest(Request request) {

        if (request.getType().equals("Type3")) {
            System.out.println("ConcreteHandler3处理了请求：" + request.getContent());
        } else if (nextHandler != null) {
            nextHandler.handleRequest(request);
        } else {
            System.out.println("无法处理该请求：" + request.getContent());
        }
    }
}
