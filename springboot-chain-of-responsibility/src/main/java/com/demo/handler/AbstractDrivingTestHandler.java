package com.demo.handler;

import com.demo.config.DrivingTestHandlerConfig;
import lombok.Getter;
import lombok.Setter;
import com.demo.model.Result;
import com.demo.model.DrivingTest;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 抽象一个责任链处理类
 *
 */
@Component
public abstract class AbstractDrivingTestHandler {

    /**
     * 当前处理器持有下一个处理器的引用
     */
    @Getter
    @Setter
    private AbstractDrivingTestHandler nextHandler;

    /**
     * 处理器执行方法
     * @param param
     * @return
     */
    public abstract Result handle(DrivingTest param);

    /**
     * 处理器配置
     */
    @Setter
    @Getter
    public DrivingTestHandlerConfig config;

    /**
     * 链路传递
     * @param param
     * @return
     */
    public Result next(DrivingTest param) {
        //下一个链路没有处理器了，直接返回
        if (Objects.isNull(nextHandler)) {
            return Result.success();
        }
        //执行下一个处理器
        return nextHandler.handle(param);
    }
}
