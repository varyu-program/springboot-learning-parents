package com.demo.handler;

import lombok.extern.slf4j.Slf4j;
import com.demo.model.DrivingTest;
import com.demo.model.Result;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 科目一处理器
 */
@Component
@Slf4j
public class Subject3Handler extends AbstractDrivingTestHandler{
    @Override
    public Result handle(DrivingTest param) {
        log.info("科目三考试得分为：" + param.getSubject3Score());
        if(Objects.isNull(param.getSubject3Score()) || param.getSubject3Score()- 90 < 0){
            return new Result(500, "您的得分是 " + param.getSubject3Score() + "，及格分 90 分，请继续努力", "");
        }
        return super.next(param);
    }
}
