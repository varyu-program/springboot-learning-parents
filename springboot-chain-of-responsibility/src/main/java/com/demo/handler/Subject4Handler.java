package com.demo.handler;

import lombok.extern.slf4j.Slf4j;
import com.demo.model.DrivingTest;
import com.demo.model.Result;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 科目一处理器
 */
@Component
@Slf4j
public class Subject4Handler extends AbstractDrivingTestHandler{
    @Override
    public Result handle(DrivingTest param) {
        log.info("科目四考试得分为：" + param.getSubject4Score());
        if(Objects.isNull(param.getSubject4Score()) || param.getSubject4Score()- 90 < 0){
            return new Result(500, "您的得分是 " + param.getSubject4Score() + "，及格分 90 分，请继续努力", "");
        }
        return super.next(param);
    }
}
