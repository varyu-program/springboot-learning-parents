package com.demo.controller;

import com.alibaba.fastjson.JSON;
import com.demo.model.DrivingTest;
import com.demo.model.Result;
import com.demo.service.DrivingTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

/**
 * com.demo.controller 测试
 */
@RestController
@RequestMapping("/drivingTest")
@Slf4j
public class DrivingTestController {

    @Autowired
    DrivingTestService drivingTestService;


    @GetMapping("/test01")
    public Result test01() {
        DrivingTest test1 = new DrivingTest();
        test1.setSubject1Score(80);
        Result r = drivingTestService.drivingTestChain(test1);
        log.info(JSON.toJSONString(r));
        return r;
    }

    @GetMapping("/test02")
    public Result test02() {
        DrivingTest test1 = new DrivingTest();
        test1.setSubject1Score(90);
        test1.setSubject2Score(80);
        Result r = drivingTestService.drivingTestChain(test1);
        log.info(JSON.toJSONString(r));
        return r;
    }

    @GetMapping("/test03")
    public Result test03() {
        DrivingTest test1 = new DrivingTest();
        test1.setSubject1Score(90);
        test1.setSubject2Score(100);
        test1.setSubject3Score(80);
        Result r = drivingTestService.drivingTestChain(test1);
        log.info(JSON.toJSONString(r));
        return r;
    }

    @GetMapping("/test04")
    public Result test04() {
        DrivingTest test1 = new DrivingTest();
        test1.setSubject1Score(90);
        test1.setSubject2Score(100);
        test1.setSubject3Score(90);
        test1.setSubject4Score(80);
        Result r = drivingTestService.drivingTestChain(test1);
        log.info(JSON.toJSONString(r));
        return r;
    }

    @GetMapping("/test05")
    public Result test05() {
        DrivingTest test1 = new DrivingTest();
        test1.setSubject1Score(90);
        test1.setSubject2Score(100);
        test1.setSubject3Score(90);
        test1.setSubject4Score(96);
        Result r = drivingTestService.drivingTestChain(test1);
        log.info(JSON.toJSONString(r));
        return r;
    }
}
