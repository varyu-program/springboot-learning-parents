package com.demo.config;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * 配置责任链中各个执行模块的执行顺序
 * 例如：
 * 科目一  --> 科目二  --> 科目三 --> 科目四
 *
 */
@AllArgsConstructor
@Data
public class DrivingTestHandlerConfig {
    /**
     * 处理器Bean名称
     */
    private String handler;
    /**
     * 下一个处理器，递归设计方式
     */
    private DrivingTestHandlerConfig next;
    /**
     * 是否降级
     */
    private Boolean down = Boolean.FALSE;
}