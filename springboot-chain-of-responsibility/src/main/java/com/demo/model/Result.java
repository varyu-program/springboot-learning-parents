package com.demo.model;

public class Result<T> {
    private Integer code;
    private String msg;
    private T data;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Result ok(T t) {
        this.setCode(200);
        this.setMsg("成功");
        this.setData(t);
        return this;
    }

    public Result fail(String msg) {
        this.setCode(500);
        this.setMsg(msg);
        return this;
    }


    public Result() {
    }

    public Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 无参返回成功
     *
     * @return
     */
    public static Result success() {
        return new Result(200,"ok","");
    }

    /**
     * 判断结果返回是否正常
     *
     * @return
     */
    public Boolean isSuccess() {
        return this.code == 200;
    }
}