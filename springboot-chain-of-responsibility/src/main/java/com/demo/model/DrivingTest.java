package com.demo.model;

import lombok.Data;

/**
 * 此处模拟了一个类似于驾校科目场景
 * 科目一通过，才可以进行科目二
 * 科目二通过，才可以进行科目三
 * 科目三通过，才可以进行科目四
 * 科目四通过，欢迎拿到驾照
 */
@Data
public class DrivingTest {
    /**
     * 科目一分数
     */
    private Integer subject1Score;
    /**
     * 科目二分数
     */
    private Integer subject2Score;
    /**
     * 科目三分数
     */
    private Integer subject3Score;
    /**
     * 科目四分数
     */
    private Integer subject4Score;

}
