package com.demo.controller;

import com.demo.custom.PrometheusCustomMonitor;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;
import java.util.Random;

@RestController
public class TestController {

    @Autowired
    private PrometheusCustomMonitor monitor;

    @Autowired
    MeterRegistry registry;

    @RequestMapping("/order")
    public String order(@RequestParam(defaultValue = "0") String flag) throws Exception {
        // 统计下单次数
        monitor.getOrderCount().increment();
        if ("1".equals(flag)) {
            throw new Exception("出错啦");
        }
        Random random = new Random();
        int amount = random.nextInt(100);
        // 统计金额
        monitor.getAmountSum().record(amount);
        return "下单成功, 金额: " + amount;
    }


    @RequestMapping("/test")
    public String test() {
        registry.counter(
                "test",
                "from","127.0.0.1",
                "method","test"
        ).increment();
        return "ok";
    }
}