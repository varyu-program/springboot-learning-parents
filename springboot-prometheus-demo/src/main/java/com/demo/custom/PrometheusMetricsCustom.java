package com.demo.custom;

import io.micrometer.core.instrument.MeterRegistry;
import io.prometheus.client.Collector;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Gauge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PrometheusMetricsCustom {

    /**
     * 请求大小指标项
     */
    private Gauge requestMax;

    /**
     * 注册到Collector上
     */
    @Autowired
    CollectorRegistry collectorRegistry;

    @PostConstruct
    public void init() {
        String labelNames[] = {"path", "method", "code"};
        requestMax = Gauge.build()
                .name("test_namespace_http_requests_max")
                .labelNames(labelNames)
                .help("Total requests.").register(collectorRegistry);
    }

    public Gauge getRequestMax() {
        return requestMax;
    }

    public void setRequestMax(Gauge requestMax) {
        this.requestMax = requestMax;
    }
}
