package com.demo.intercepter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 和springmvc的webmvc拦截配置一样
 *
 */
@Configuration
public class WebConfigurer implements WebMvcConfigurer {

    @Autowired
    PrometheusMetricsCounterInterceptor prometheusMetricsCounterInterceptor;
    @Autowired
    PrometheusMetricsGaugeInterceptor prometheusMetricsGaugeInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截所有请求，通过判断是否有 @LoginRequired 注解 决定是否需要登录
        registry.addInterceptor(prometheusMetricsCounterInterceptor).addPathPatterns("/**");
        registry.addInterceptor(prometheusMetricsGaugeInterceptor).addPathPatterns("/**");
    }
}