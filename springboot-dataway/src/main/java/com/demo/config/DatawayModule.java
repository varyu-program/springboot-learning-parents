package com.demo.config;

import net.hasor.core.ApiBinder;
import net.hasor.core.DimModule;
import net.hasor.db.JdbcModule;
import net.hasor.db.Level;
import net.hasor.spring.SpringModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;


/**
 * @Classname DatawayModule
 * @Description 将Hasor模块注入spring，并注入数据源
 * @Date 2023/7/28 11:43
 * @Created by Leo825
 */
@DimModule  // Hasor 中的标签，表明是一个Hasor的model
@Component  // Spring 中的标签，表明是一个组件
public class DatawayModule implements SpringModule {
    @Autowired
    private DataSource dataSource;

    //这里注入数据源
    @Override
    public void loadModule(ApiBinder apiBinder) throws Throwable {
        // .DataSource form Spring boot into Hasor
        apiBinder.installModule(new JdbcModule(Level.Full, this.dataSource));
    }
}
