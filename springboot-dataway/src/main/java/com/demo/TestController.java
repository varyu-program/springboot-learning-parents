package com.demo;

import com.demo.config.DatawayModule;
import net.hasor.core.ApiBinder;
import net.hasor.core.AppContext;
import net.hasor.core.Environment;
import net.hasor.core.Hasor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private DatawayModule datawayModule;

    @GetMapping("/test")
    public String getData() {
        AppContext build = Hasor.create().build(datawayModule);
        AppContext appContext = Hasor.create().build();
        //获得环境变量
        Environment env = appContext.getEnvironment();
//        System.out.println(env.getVariable("java.version"));
//        build.getInstance()
        return env.getVariable("java.version");
    }


}
