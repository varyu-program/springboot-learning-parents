package com.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.model.User;

/**
 * 用户表
 */
public interface UserMapper extends BaseMapper<User> {

}
