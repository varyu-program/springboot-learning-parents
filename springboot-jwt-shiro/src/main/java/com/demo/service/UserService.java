package com.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.model.User;

import javax.security.sasl.AuthenticationException;


public interface UserService extends IService<User> {
    User getUser(String username) throws AuthenticationException;
}
