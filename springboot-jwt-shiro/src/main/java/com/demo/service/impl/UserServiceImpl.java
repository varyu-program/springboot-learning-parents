package com.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.demo.mapper.UserMapper;
import com.demo.model.User;
import com.demo.service.UserService;
import org.springframework.stereotype.Service;
import javax.security.sasl.AuthenticationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    public User getUser(String username) throws AuthenticationException {
        Map<String,Object> columnMap = new HashMap<>();
        columnMap.put("username", username);
        List<User> users = baseMapper.selectByMap(columnMap);
        if(users.isEmpty()){
            throw new AuthenticationException("User didn't existed!");
        }
        return users.get(0);
    }
}
