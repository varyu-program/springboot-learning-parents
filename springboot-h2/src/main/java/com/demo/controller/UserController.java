package com.demo.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.demo.base.Result;
import com.demo.entity.User;
import com.demo.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * com.demo.controller 测试
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {

    /**
     * 构造方法注入
     */
    @Autowired
    IUserService userService;


    /**
     * 保存数据
     *
     * @return
     */
    @GetMapping("/save")
    public Result save() {
        User user = new User();
        user.setId(10);
        user.setUsername("miaolinlin");
        user.setPwd("121212");
        userService.save(user);
        return new Result().ok(user);
    }

    /**
     * 修改数据
     *
     * @param id
     * @return
     */
    @GetMapping("/update")
    public Result update(Integer id) {
        User user = new User();
        user.setId(id);
        user.setPwd("1111111111");
        userService.updateById(user);
        return new Result().ok("{}");
    }

    @GetMapping("/list")
    public Result list() {
        // 返回所有
        List<User> list = userService.list();
        return new Result().ok(list);
    }

    @GetMapping("/listByContion")
    public Result listByContion() {
        /**
         * 条件查询， 通过QueryWrapper来实现查询的条件：
         * eq: 代表相等
         * like: 模糊匹配
         * orderBy: 排序
         * in, notin
         * 大于，小于，between等
         */
        List<User> list = userService.list(new LambdaQueryWrapper<User>()
                // 查询年龄=11的
                .eq(User::getUsername, "miao")
                // 模糊匹配
                .like(User::getPwd, "%111%")
                // 排序，按照创建时间
                .orderByDesc(User::getCreateTime)
        );
        return new Result().ok(list);
    }

    /**
     * 根据id获取数据
     *
     * @param id
     * @return
     */
    @GetMapping("/getById")
    public Result getById(Integer id) {
        User user = userService.getById(id);
        return new Result().ok(user);
    }

    /**
     * 删除数据
     *
     * @param id
     * @return
     */
    @GetMapping("/delete")
    public Result delete(Integer id) {
        userService.removeById(id);
        return new Result().ok("success");
    }

    /**
     * 分页查询
     *
     * @param pageNum
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public Result page(int pageNum, int pageSize, String name) {
        IPage<User> page = new Page<>(pageNum, pageSize);

        IPage<User> page1 = userService.page(page, new LambdaQueryWrapper<User>()
                // 主要演示这里可以加条件。在name不为空的时候执行
                .like(StringUtils.isNotEmpty(name), User::getUsername, "%" + name + "%"));

        return new Result().ok(page1);
    }
}
