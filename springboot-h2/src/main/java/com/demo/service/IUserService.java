package com.demo.service;

import com.demo.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author Leo825
 * @since 2022-07-05
 */
public interface IUserService extends IService<User> {

}
