package com.demo;

import com.demo.util.MinioUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

@SpringBootTest
@Slf4j
class SpringbootMinioApplicationTests {
    @Autowired
    MinioUtils minioUtils;
    @Test
    void contextLoads() {
    }
    @Test
    public void testGetAllBucket() {
        minioUtils.getAllBucket();
    }
    @Test
    public void testUploadToMinio() {
        String filePath = "C:\\Users\\Gavin\\Pictures\\Background\\air.jpg";
        try (InputStream inputStream = new FileInputStream(filePath)) {
            minioUtils.uploadToMinio(inputStream, "air");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetUrlByObjectName() {
        log.info(minioUtils.getUrlByObjectName("air"));
    }
    @Test
    public void testDownloadFromMinioToFile() {
        //要下载的文件夹名
        String objName = "air";
        //要下载的文件名称
        String fileName = "air.jpg";
        //路径
        String dir = "D:\\minioDownload";
        minioUtils.downloadFromMinioToFile(objName, fileName, dir);
    }
}
