package com.demo.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class User implements Serializable {
    Integer id;
    String userName;
    String pwd;
    Date createTime;
    Date updateTime;
}
