package com.demo.service;

import com.demo.model.User;

import java.util.List;

public interface UserService {
    /**
     * 新增
     * @param user
     */
    void addUser(User user);

    /**
     * 删除用户
     * @param id
     */
    void deleteUserById(Integer id);

    /**
     * 更新用户
     * @param user
     * @return
     */
    void updateUser(User user);

    /**
     * 查询用户
     * @param ids
     * @return
     */
    List<User> getUsersById(List<Integer> ids);
}
