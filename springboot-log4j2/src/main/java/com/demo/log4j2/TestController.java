package com.demo.log4j2;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TestController {
    /**
     * 获取一个日志组件,也可以使用lombok的@Slf4j注解
     */
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/helloLog4j2")
    public String testDemo() {
        log.trace("hello trace");
        log.debug("hello debug");
        log.info("hello info");
        log.warn("hello warn");
        log.error("hello error");
        return "Hello Log4j2!";
    }

    @GetMapping("/testCostTime")
    public String testCostTime() {
        long sum = 0;
        for (int i = 0; i < 5; i++) {
            sum += costTime();
        }
        return "log4j cost time " + (sum / 5) + " ms";
    }


    /**
     * 测试单次耗时
     *
     * @return
     */
    private long costTime() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 50000; i++) {
            log.info("hello info");
        }
        long end = System.currentTimeMillis();
        return end - start;
    }
}