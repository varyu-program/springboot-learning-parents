package com.demo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProvinceVo implements Serializable {
    /**
     * 省份id
     */
    String provinceId;
    /**
     * 省份名称
     */
    String province;
    /**
     * 地市id
     */
    String cityId;
    /**
     * 地市名称
     */
    String city;
}
