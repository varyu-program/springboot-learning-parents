package com.demo.controller;

import com.demo.base.Result;
import com.demo.vo.ProvinceVo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * com.demo.controller 测试
 *
 */
@RestController
@RequestMapping("/province")
public class ProvinceController {

    /**
     *
     * @param provinceVo
     * @return
     */
    @PostMapping("/getProvinceId")
    public Result getProvinceId(@RequestBody ProvinceVo provinceVo) {
        return new Result().ok(provinceVo);
    }
}
