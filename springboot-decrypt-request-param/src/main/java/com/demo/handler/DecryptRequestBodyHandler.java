package com.demo.handler;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.IoUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 * 统一处理请求参数
 *
 * @author leo825
 * @date 2022/11/23 11:17
 */
@RestControllerAdvice
@Slf4j
public class DecryptRequestBodyHandler implements RequestBodyAdvice {

    /**
     *  省份编码的别称，注意：provinceOrCityId 级别是1的时候代表省份
     */
   public static String[] provinceIdNickNames = {"provinceId", "provinceOrCityId"};

    /**
     * 该方法用于判断当前请求，是否要执行beforeBodyRead方法
     * methodParameter方法的参数对象
     * type方法的参数类型
     * aClass 将会使用到的Http消息转换器类类型
     * 注意：此判断方法，会在beforeBodyRead 和 afterBodyRead方法前都触发一次。
     *
     * @return 返回true则会执行beforeBodyRead
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    /**
     * 在Http消息转换器执转换，之前执行
     * inputMessage 客户端的请求数据
     * parameter方法的参数对象
     * targetType方法的参数类型
     * converterType 将会使用到的Http消息转换器类类型
     * @return 返回 一个自定义的HttpInputMessage
     */
    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
        // 如果body是空内容直接返回原来的请求
        if (inputMessage.getBody().available() <= 0) {
            return inputMessage;
        }
        // 判断文件头中不包含 encryKey 则跳过
        HttpHeaders headers = inputMessage.getHeaders();
        if (headers.get("encryKey") == null) {
            return inputMessage;
        }
        // 判断关键字 encryKey 的值是否合法或者是全国权限， MTAw 代表的是全国权限 100
        String encryKey = headers.get("encryKey").get(0);
        if (StringUtils.isBlank(encryKey) || StringUtils.isNumeric(encryKey)) {
            return inputMessage;
        }
        // 将输入流读出来，注意 body 里面的流只能读一次
        ByteArrayOutputStream requestBodyDataByte = new ByteArrayOutputStream();
        try {
            IoUtil.copy(inputMessage.getBody(), requestBodyDataByte);
        } catch (Exception e) {
            log.error("参数流拷贝失败: ", e);
            return inputMessage;
        }
        // 对包含 encryKey 关键字的接口进行处理
        ByteArrayOutputStream requestBodyDataByteNew = null;
        try {
            String provinceId = Base64.decodeStr(encryKey);
            log.info("encryKey:{},  provinceId:{}", encryKey, provinceId);
            JSONObject body = JSON.parseObject(requestBodyDataByte.toByteArray(), JSONObject.class);
            Arrays.asList(provinceIdNickNames).stream().forEach(e -> {
                if (body.containsKey(e)) {
                    body.put(e, provinceId);
                }
            });
            log.info("request: " + body.toJSONString());
            requestBodyDataByteNew = new ByteArrayOutputStream();
            IoUtil.copy(new ByteArrayInputStream(body.toJSONString().getBytes()), requestBodyDataByteNew);
        } catch (Throwable e) {
            log.error("encryKey 转换异常 ", e);
        }
        // 如果上述发生异常，仍然使用原来的请求内容
        requestBodyDataByte = requestBodyDataByteNew != null ? requestBodyDataByteNew : requestBodyDataByte;
        InputStream rawInputStream = new ByteArrayInputStream(requestBodyDataByte.toByteArray());
        inputMessage.getHeaders().set(HttpHeaders.CONTENT_LENGTH, String.valueOf(rawInputStream.available()));
        return new HttpInputMessage() {
            @Override
            public HttpHeaders getHeaders() {
                return inputMessage.getHeaders();
            }

            @Override
            public InputStream getBody() throws IOException {
                return rawInputStream;
            }
        };
    }

    /**
     * 在Http消息转换器执转换，之后执行
     * body 转换后的对象
     * inputMessage 客户端的请求数据
     * parameter handler方法的参数类型
     * targetType handler方法的参数类型
     * converterType 使用的Http消息转换器类类型
     * @return 返回一个新的对象
     */
    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }

    /**
     * 参数与afterBodyRead相同，不过这个方法处理的是，body为空的情况
     */
    @Override
    public Object handleEmptyBody(@Nullable Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }
}
