package com.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.entity.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Leo825
 * @since 2022-07-05
 */
public interface UserMapper extends BaseMapper<User> {

}
